import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient:DestinosApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];

    this.store.select(state => state.destinations.favorito)
      .subscribe(data => {
        const fav = data;
        if (data != null) {
          this.updates.push("Se ha elegido a " + data.name)
        }
      });
    
    store.select(state => state.destinations.items).subscribe(items => this.all = items);

    /*this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
      if (d != null) {
        this.updates.push("Se ha elegido a " + d.name)
      }
    });*/
  }

  ngOnInit(): void {
  }

  add(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  selected(d: DestinoViaje) {
    this.destinosApiClient.select(d);
  }

}
