import { DestinoViaje } from './destino-viaje.model';
import { AppConfig, AppState, APP_CONFIG, db } from './../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import { forwardRef, Inject, Injectable }  from '@angular/core';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
	destinations: DestinoViaje[];
	//current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
	constructor(private store: Store<AppState>, 
		@Inject(forwardRef(() => APP_CONFIG )) private config: AppConfig,
		private http: HttpClient) {
		   
		this.store
			.select(state => state.destinations)
			.subscribe((data) => {
				console.log('destinos sub store');
				console.log(data);
				this.destinations = data.items;
			});

		this.store
			.subscribe((data) => {
				console.log('all store');
				console.log(data);
			});
	}
	add(d: DestinoViaje){
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.name }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if (data.status === 200) {
				this.store.dispatch(new NuevoDestinoAction(d));
				// Add to db
				const myDb = db;
				myDb.destinos.add(d);
				console.log('todos los destinos de la db!');
				myDb.destinos.toArray().then(destinos => console.log(destinos));
			}
		});
	}
	getAll(){
	  	return this.destinations;
	}
	getById(id: string): DestinoViaje {
		/*return this.destinations.filter(function(d) {
			return d.isSelected.toString() === id;
		})[0];*/
		/*const myDb = db;
		myDb.destinos.filter(function(d) {
			return d.id === id;
		})[0];*/
		return null;
	}
	select(d: DestinoViaje) {
		/*this.destinations.forEach(d => d.setSelected(false));
		d.setSelected(true);*/
		this.store.dispatch(new ElegidoFavoritoAction(d));
		//this.current.next(d);
	}
	/*subscribeOnChange(fn) {
		this.current.subscribe(fn); 
	}*/
}