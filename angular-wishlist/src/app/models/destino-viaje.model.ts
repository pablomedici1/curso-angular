import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	private selected: boolean;
	public services: string[];
	id = uuid();
	constructor(public name: string, public imageUrl: string, public votes: number = 0) {
		this.services = ['pileta', 'desayuno'];
	}

	isSelected(): boolean {
		return this.selected;
	}

	setSelected(isSelected: boolean) {
		this.selected = isSelected;
	}

	voteUp() {
		this.votes++;
	}

	votesDown() {
		this.votes--;
	}
}