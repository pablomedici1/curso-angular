describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
      cy.visit('http://localhost:4200');
      cy.contains('angular-wishlist');
      cy.get('h1 b').should('contain', 'HOLA es');
    });

    it('ingreso de Buenos Aires', () => {
        cy.get('.form-group').find('[id="nombre"]').type('Buenos Aires');
        cy.get('form').find('[type=submit]').click();
            //.next().should('contain', 'Your form has been submitted!')
        cy.get('.card-desc h3').should('contain', 'Buenos Aires')
    });

    it('actividad Buenos Aires', () => {
        cy.contains('Se ha elegido a Buenos Aires');
    });

  });